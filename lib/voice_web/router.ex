defmodule VoiceWeb.Router do
  use VoiceWeb, :router

  pipeline :api do
    plug(:accepts, ["json"])
  end

  scope "/api", VoiceWeb do
    pipe_through(:api)
  end
end
