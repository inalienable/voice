use Mix.Config

config :voice, ecto_repos: [Voice.Repo]

config :voice, VoiceWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "2eHS6odT+Eru3WPEUHQ7eG3Qe86/WRGlJyu7HFB7OqULrB9qcp21XQWOqbbqKC+k",
  render_errors: [view: VoiceWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: Voice.PubSub, adapter: Phoenix.PubSub.PG2]

config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

import_config "#{Mix.env()}.exs"
